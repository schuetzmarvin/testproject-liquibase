# Setup 

After the repository has been cloned, make sure that the ``username`` and ``password`` for database access, as well as the ``port`` on which the application is running, are set in the application.properties file.
