package de.testproject.liquibase.web;



import de.testproject.liquibase.domain.book.BookService;
import de.testproject.liquibase.domain.book.dto.BookEntityCreateRequestDto;
import de.testproject.liquibase.domain.book.dto.BookEntityUpdateRequestDto;
import de.testproject.liquibase.domain.book.model.Book;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/book")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    public Book getBook(@PathVariable UUID id){
        return bookService.getBookById(id);
    }

    @Transactional
    @PostMapping
    public Book addNewBook(@RequestBody BookEntityCreateRequestDto bookEntityCreateRequestDto){
        return bookService.addNewBook(bookEntityCreateRequestDto);
    }

    @Transactional
    @PutMapping("/{id}")
    public Book updateBook(@PathVariable UUID id, @RequestBody BookEntityUpdateRequestDto bookEntityUpdateRequestDto){
        return bookService.updateBook(id,bookEntityUpdateRequestDto);
    }

    @Transactional
    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable UUID id){
        bookService.deleteBook(id);
    }

}
