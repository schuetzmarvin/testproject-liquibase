package de.testproject.liquibase.domain.book;

import de.testproject.liquibase.domain.book.dto.BookEntityCreateRequestDto;
import de.testproject.liquibase.domain.book.dto.BookEntityUpdateRequestDto;
import de.testproject.liquibase.domain.book.entity.BookEntity;
import de.testproject.liquibase.domain.book.model.Book;
import de.testproject.liquibase.domain.book.repository.BookRepository;
import de.testproject.liquibase.exceptions.InputValidationException;
import de.testproject.liquibase.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    public List<Book> getAllBooks(){
        return bookRepository.findAll()
                .stream()
                .map(this::createBook)
                .collect(Collectors.toList());
    }

    public Book getBookById(UUID id){
        return bookRepository.findById(id)
                .map(this::createBook)
                .orElseThrow(() -> new ObjectNotFoundException("There is no book with id: " + id));
    }

    public Book addNewBook(BookEntityCreateRequestDto bookEntityCreateRequestDto){
        if(bookEntityCreateRequestDto.getTitle().isBlank() || bookEntityCreateRequestDto.getAuthor().isBlank()){
            throw new InputValidationException("Either Title or Author are blank, which is not allowed!");
        }

        final boolean nameExists = bookRepository.existsByTitle(bookEntityCreateRequestDto.getTitle());
        final boolean yearExists = bookRepository.existsByFromYear(bookEntityCreateRequestDto.getFromYear());

        if(nameExists && yearExists){
            throw new InputValidationException("A book with the given year and title already exists.");
        }

        if(Objects.equals(validateDate(bookEntityCreateRequestDto.getFromYear()),false)){
            throw new InputValidationException("Year is not in the correct format or is in an invalid period (before 1600).");
        }

        BookEntity newBooK = new BookEntity(
                bookEntityCreateRequestDto.getTitle(),
                bookEntityCreateRequestDto.getAuthor(),
                bookEntityCreateRequestDto.getFromYear()
        );

        bookRepository.save(newBooK);
        return createBook(newBooK);
    }

    public Book updateBook(UUID id, BookEntityUpdateRequestDto bookEntityUpdateRequestDto){
        BookEntity book = bookRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("There is no book with id: " + id));
        final boolean nameExists = bookRepository.existsByTitle(book.getTitle());
        final boolean yearExists = bookRepository.existsByFromYear(book.getFromYear());

        if(nameExists && yearExists){
            throw new InputValidationException("A book with the given year and title already exists.");
        }

        if(Objects.equals(book.getFromYear(),bookEntityUpdateRequestDto.getFromYear())){
            throw new InputValidationException("The year of the book is already " + bookEntityUpdateRequestDto.getFromYear());
        }

        if(Objects.equals(validateDate(bookEntityUpdateRequestDto.getFromYear()),false)){
            throw new InputValidationException("Year is not in the correct format or is in an invalid period (before 1600).");
        }

        book.setFromYear(bookEntityUpdateRequestDto.getFromYear());

        bookRepository.save(book);
        return createBook(book);
    }

    public void deleteBook(UUID id){
        bookRepository.deleteById(id);
    }

    private boolean validateDate(int fromYear) {
        Pattern yearDigitPattern = Pattern.compile("^[0-9]{4}$");
        Matcher matcher = yearDigitPattern.matcher(Integer.toString(fromYear));
        boolean isValidYearFormat = matcher.matches();

        if(isValidYearFormat){
            return fromYear >= 1600;
        }

        return false;
    }


    private Book createBook(BookEntity bookEntity){
        return new Book(
                bookEntity.getId(),
                bookEntity.getTitle(),
                bookEntity.getAuthor(),
                bookEntity.getFromYear());
    }
}
