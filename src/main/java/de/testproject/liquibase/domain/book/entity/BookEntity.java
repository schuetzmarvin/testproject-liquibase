package de.testproject.liquibase.domain.book.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "books")
public class BookEntity {
    @Id
    private UUID id = UUID.randomUUID();

    private String title;

    private String author;

    @Column(name = "from_year")
    private int fromYear;

    public BookEntity(){

    }

    public BookEntity(String title, String author, int fromYear){
        this.title = title;
        this.author = author;
        this.fromYear = fromYear;
    }

    public UUID getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public int getFromYear() {
        return this.fromYear;
    }

    public void setFromYear(int fromYear) {
        this.fromYear = fromYear;
    }
}
