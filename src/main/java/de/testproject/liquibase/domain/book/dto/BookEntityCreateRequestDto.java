package de.testproject.liquibase.domain.book.dto;

public class BookEntityCreateRequestDto {
    private String title;
    private String author;
    private int fromYear;

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }


    public int getFromYear() {
        return this.fromYear;
    }

}
