package de.testproject.liquibase.domain.book.repository;

import de.testproject.liquibase.domain.book.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, UUID> {

    boolean existsByTitle(String title);
    boolean existsByFromYear(int year);
}
